﻿namespace gorsellokantatakip2a
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAdi = new System.Windows.Forms.TextBox();
            this.txtSoyadi = new System.Windows.Forms.TextBox();
            this.btnGiris = new System.Windows.Forms.Button();
            this.lblHata = new System.Windows.Forms.Label();
            this.gvMüsteri2 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHata2 = new System.Windows.Forms.Label();
            this.lbnSonkayit = new System.Windows.Forms.Button();
            this.btnDuzen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvMüsteri2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtAdi
            // 
            this.txtAdi.Location = new System.Drawing.Point(147, 26);
            this.txtAdi.Name = "txtAdi";
            this.txtAdi.Size = new System.Drawing.Size(100, 20);
            this.txtAdi.TabIndex = 0;
            // 
            // txtSoyadi
            // 
            this.txtSoyadi.Location = new System.Drawing.Point(332, 29);
            this.txtSoyadi.Name = "txtSoyadi";
            this.txtSoyadi.Size = new System.Drawing.Size(100, 20);
            this.txtSoyadi.TabIndex = 1;
            // 
            // btnGiris
            // 
            this.btnGiris.Location = new System.Drawing.Point(470, 27);
            this.btnGiris.Name = "btnGiris";
            this.btnGiris.Size = new System.Drawing.Size(75, 23);
            this.btnGiris.TabIndex = 2;
            this.btnGiris.Text = "Listele";
            this.btnGiris.UseVisualStyleBackColor = true;
            this.btnGiris.Click += new System.EventHandler(this.btnGiris_Click);
            // 
            // lblHata
            // 
            this.lblHata.AutoSize = true;
            this.lblHata.Location = new System.Drawing.Point(102, 164);
            this.lblHata.Name = "lblHata";
            this.lblHata.Size = new System.Drawing.Size(0, 13);
            this.lblHata.TabIndex = 3;
            // 
            // gvMüsteri2
            // 
            this.gvMüsteri2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvMüsteri2.Location = new System.Drawing.Point(82, 146);
            this.gvMüsteri2.Name = "gvMüsteri2";
            this.gvMüsteri2.Size = new System.Drawing.Size(752, 150);
            this.gvMüsteri2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Adı :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Soyadı :";
            // 
            // lblHata2
            // 
            this.lblHata2.AutoSize = true;
            this.lblHata2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHata2.Location = new System.Drawing.Point(239, 117);
            this.lblHata2.Name = "lblHata2";
            this.lblHata2.Size = new System.Drawing.Size(0, 13);
            this.lblHata2.TabIndex = 7;
            // 
            // lbnSonkayit
            // 
            this.lbnSonkayit.Location = new System.Drawing.Point(470, 80);
            this.lbnSonkayit.Name = "lbnSonkayit";
            this.lbnSonkayit.Size = new System.Drawing.Size(90, 26);
            this.lbnSonkayit.TabIndex = 8;
            this.lbnSonkayit.Text = "son kayıdı getir";
            this.lbnSonkayit.UseVisualStyleBackColor = true;
            this.lbnSonkayit.Click += new System.EventHandler(this.lbnSonkayit_Click);
            // 
            // btnDuzen
            // 
            this.btnDuzen.Location = new System.Drawing.Point(158, 82);
            this.btnDuzen.Name = "btnDuzen";
            this.btnDuzen.Size = new System.Drawing.Size(75, 23);
            this.btnDuzen.TabIndex = 9;
            this.btnDuzen.Text = "Düzenle";
            this.btnDuzen.UseVisualStyleBackColor = true;
            this.btnDuzen.Click += new System.EventHandler(this.btnDuzen_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 337);
            this.Controls.Add(this.btnDuzen);
            this.Controls.Add(this.lbnSonkayit);
            this.Controls.Add(this.lblHata2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gvMüsteri2);
            this.Controls.Add(this.lblHata);
            this.Controls.Add(this.btnGiris);
            this.Controls.Add(this.txtSoyadi);
            this.Controls.Add(this.txtAdi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvMüsteri2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAdi;
        private System.Windows.Forms.TextBox txtSoyadi;
        private System.Windows.Forms.Button btnGiris;
        private System.Windows.Forms.Label lblHata;
        private System.Windows.Forms.DataGridView gvMüsteri2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblHata2;
        private System.Windows.Forms.Button lbnSonkayit;
        private System.Windows.Forms.Button btnDuzen;
    }
}