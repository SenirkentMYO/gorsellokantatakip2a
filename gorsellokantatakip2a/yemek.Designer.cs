﻿namespace gorsellokantatakip2a
{
    partial class yemek
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGonder = new System.Windows.Forms.Button();
            this.btnMusteri = new System.Windows.Forms.Button();
            this.btnAdisyon = new System.Windows.Forms.Button();
            this.cmCorba = new System.Windows.Forms.ComboBox();
            this.cmYemek = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmTatli = new System.Windows.Forms.ComboBox();
            this.btnHesap = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Çorba";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Yemek";
            // 
            // btnGonder
            // 
            this.btnGonder.Location = new System.Drawing.Point(34, 166);
            this.btnGonder.Name = "btnGonder";
            this.btnGonder.Size = new System.Drawing.Size(75, 23);
            this.btnGonder.TabIndex = 2;
            this.btnGonder.Text = "Gönder";
            this.btnGonder.UseVisualStyleBackColor = true;
            this.btnGonder.Click += new System.EventHandler(this.btnGonder_Click);
            // 
            // btnMusteri
            // 
            this.btnMusteri.Location = new System.Drawing.Point(116, 165);
            this.btnMusteri.Name = "btnMusteri";
            this.btnMusteri.Size = new System.Drawing.Size(75, 23);
            this.btnMusteri.TabIndex = 3;
            this.btnMusteri.Text = "Müşteri Ekle";
            this.btnMusteri.UseVisualStyleBackColor = true;
            this.btnMusteri.Click += new System.EventHandler(this.btnMusteri_Click);
            // 
            // btnAdisyon
            // 
            this.btnAdisyon.Location = new System.Drawing.Point(198, 165);
            this.btnAdisyon.Name = "btnAdisyon";
            this.btnAdisyon.Size = new System.Drawing.Size(75, 23);
            this.btnAdisyon.TabIndex = 4;
            this.btnAdisyon.Text = "Adisyon";
            this.btnAdisyon.UseVisualStyleBackColor = true;
            this.btnAdisyon.Click += new System.EventHandler(this.btnAdisyon_Click);
            // 
            // cmCorba
            // 
            this.cmCorba.FormattingEnabled = true;
            this.cmCorba.Items.AddRange(new object[] {
            "Mercimek Çorbası",
            "Ezogelin Çorba",
            "Domates Çorbası",
            "Yayla Çorbası",
            "Mantar Çorbası"});
            this.cmCorba.Location = new System.Drawing.Point(131, 12);
            this.cmCorba.Name = "cmCorba";
            this.cmCorba.Size = new System.Drawing.Size(121, 21);
            this.cmCorba.TabIndex = 5;
            this.cmCorba.Tag = "";
            this.cmCorba.Text = "Çorba Seçin";
            // 
            // cmYemek
            // 
            this.cmYemek.FormattingEnabled = true;
            this.cmYemek.Items.AddRange(new object[] {
            "Tavuk Sote",
            "İzmir Köfte",
            "K. Fasulye",
            "P. Pilavı",
            "T. Fasulye",
            "Bakla"});
            this.cmYemek.Location = new System.Drawing.Point(131, 47);
            this.cmYemek.Name = "cmYemek";
            this.cmYemek.Size = new System.Drawing.Size(121, 21);
            this.cmYemek.TabIndex = 6;
            this.cmYemek.Text = "Ana Yemek Seçin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tatlı";
            // 
            // cmTatli
            // 
            this.cmTatli.FormattingEnabled = true;
            this.cmTatli.Items.AddRange(new object[] {
            "Fırın Sütlaç",
            "Kabak Tatlısı",
            "Kadayıf Tatlısı",
            "Tulumba Tatlısı"});
            this.cmTatli.Location = new System.Drawing.Point(131, 83);
            this.cmTatli.Name = "cmTatli";
            this.cmTatli.Size = new System.Drawing.Size(121, 21);
            this.cmTatli.TabIndex = 8;
            this.cmTatli.Text = "Tatlı Seçin";
            // 
            // btnHesap
            // 
            this.btnHesap.Location = new System.Drawing.Point(116, 211);
            this.btnHesap.Name = "btnHesap";
            this.btnHesap.Size = new System.Drawing.Size(75, 23);
            this.btnHesap.TabIndex = 9;
            this.btnHesap.Text = "Hesap Kes";
            this.btnHesap.UseVisualStyleBackColor = true;
            this.btnHesap.Click += new System.EventHandler(this.btnHesap_Click);
            // 
            // yemek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 269);
            this.Controls.Add(this.btnHesap);
            this.Controls.Add(this.cmTatli);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmYemek);
            this.Controls.Add(this.cmCorba);
            this.Controls.Add(this.btnAdisyon);
            this.Controls.Add(this.btnMusteri);
            this.Controls.Add(this.btnGonder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "yemek";
            this.Text = "yemek";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGonder;
        private System.Windows.Forms.Button btnMusteri;
        private System.Windows.Forms.Button btnAdisyon;
        private System.Windows.Forms.ComboBox cmCorba;
        private System.Windows.Forms.ComboBox cmYemek;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmTatli;
        private System.Windows.Forms.Button btnHesap;
    }
}