﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class Anasayfa : Form
    {
        public Anasayfa()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MusteriEkle yeniform = new MusteriEkle();
            yeniform.Show();
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
           Form1 yeniform = new Form1();
            yeniform.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            PersonelTakibi yeniform = new PersonelTakibi();
            yeniform.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AdisyonTakibi yeniform = new AdisyonTakibi();
            yeniform.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StokVeUrunler yeniform = new StokVeUrunler();
            yeniform.Show();
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            EkGiderler yeniform = new EkGiderler();
            yeniform.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            KasaTakibi yeniform = new KasaTakibi();
            yeniform.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FaturaTakibi yeniform = new FaturaTakibi();
            yeniform.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Masalar yeniform = new Masalar();
            yeniform.Show();
        }
    }
}
