﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class PersonelTakibi : Form
    {
        public PersonelTakibi()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
       

        private void PersonelTakibi_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            PersonelKayit yeniform = new PersonelKayit();
            yeniform.Show();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtad_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnara_Click(object sender, EventArgs e)
        {
            List<Personel> kullanicilist = new List<Personel>();
            LokantaDBEntities2 database = new LokantaDBEntities2();
            kullanicilist = database.Personel.Where(a => a.Ad == txtad.Text | a.PersonelNo == txtno.Text).ToList();
            if (kullanicilist.Count > 0)
            {
                List<Personel> MüsteriList = database.Personel.Where(tablom => tablom.Ad.Contains(txtad.Text)).ToList();

                gvpersonel.DataSource = kullanicilist;
                lblHata.Text = "Aranılan kişi yukarıda tabloda gösterilmektedir.";
            }
            else
            {
             

             if (txtad.Text == "")
             {
                 lblHata.Text = "Lokantamızda - " + txtno.Text + " - numaralı personel bulunmamaktadır.";
             }
             else
                 lblHata.Text = "Lokantamızda - " + txtad.Text + " - adında personel bulunmamaktadır";
            
            }
        }

        private void btn_duzenle_Click(object sender, EventArgs e)
        {
            LokantaDBEntities2 db = new LokantaDBEntities2();
            int id = int.Parse(gvpersonel.SelectedRows[0].Cells[0].Value.ToString());
            Personel ktp = db.Personel.Where(a => a.ID == id).First();

            KisiDuzenle f = new KisiDuzenle(ktp);
            f.Show();
            this.Hide();
        }
    }
}
