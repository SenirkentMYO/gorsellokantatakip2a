﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class KisiDuzenle : Form
    {
        Personel prsnl;
        public KisiDuzenle(Personel k)
        {
            InitializeComponent();
           prsnl=k;
        }

        private void KisiDuzenle_Load(object sender, EventArgs e)
        {
            txtID.Text = prsnl.ID.ToString();//
            txtPersonelNo.Text = prsnl.PersonelNo;//
            txtAd.Text = prsnl.Ad;//
            txtSoyadı.Text = prsnl.Soyad;//
            txtCepTel.Text = prsnl.CepTel;//
            txtAdres.Text = prsnl.Adres;//
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PersonelTakibi yeniform = new PersonelTakibi();
            yeniform.Show();
            this.Hide();
        }

        private void btn_duzenle_Click(object sender, EventArgs e)
        {

        }
    }
}
