﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class KisiDuzenle2 : Form
    {
        Müsteri mstr;
        public KisiDuzenle2(Müsteri k)
        {
            InitializeComponent();
            mstr = k;
        }

        private void KisiDuzenle2_Load(object sender, EventArgs e)
        {
            txtId.Text = mstr.ID.ToString();//
            txtAdi.Text = mstr.Adi;//

            txtSoyadi.Text = mstr.Soyadi;//
            txtTel.Text = mstr.Tel;//
            txtAdres.Text = mstr.Adres;//
            txtMail.Text = mstr.Email;//
        }
    }
}
