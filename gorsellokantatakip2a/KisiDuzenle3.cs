﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class KisiDuzenle3 : Form
    {
        Fatura mstr;
        public KisiDuzenle3(Fatura k)
        {
            InitializeComponent();
            mstr = k;
        }

        private void KisiDuzenle3_Load(object sender, EventArgs e)
        {
            txtID.Text = mstr.ID.ToString();//
            txtAdi.Text = mstr.Adi;//

            txtSoyadi.Text = mstr.Soyadi;//
            txtUrunAdi.Text = mstr.UrunAdi;//
            txtMiktari.Text = mstr.Miktari;//
            txtBirim.Text = mstr.Birim;//

            txtBirimFiyati.Text = mstr.BirimFiyati;//
            txtKDV.Text = mstr.Kdv;//
            txtKDVTutari.Text = mstr.KdvTutari.ToString();//
            txtToplamTutar.Text = mstr.ToplamTutar.ToString();//
            //txtSatisTarihi.Text = mstr.SatisTarihi;//
            txtFaturaNo.Text = mstr.FaturaNo.ToString();//
        }
    }
}
