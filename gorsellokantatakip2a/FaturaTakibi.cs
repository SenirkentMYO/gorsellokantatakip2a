﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class FaturaTakibi : Form
    {
        public FaturaTakibi()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (LokantaDBEntities2 dbase = new LokantaDBEntities2())
            {
                List<Fatura> kitapList = dbase.Fatura.ToList();


                dataGridView1.DataSource = kitapList;

            } 
        }

        private void FaturaTakibi_Load(object sender, EventArgs e)
        {
            using (LokantaDBEntities2 dbase = new LokantaDBEntities2())
            {
                List<Fatura> faturaList = dbase.Fatura.ToList();
                for (int i = 0; i < faturaList.Count(); i++)
                {
                    faturaList[i].ToplamTutar = faturaList[i].ToplamTutar + faturaList[i].ToplamTutar * faturaList[i].KdvTutari/ 100;        
            }


                int KDVsizToplam = dbase.Fatura.Sum(a => (int)a.ToplamTutar);//veritabanınadan veri çekme
                txtKdv.Text = KDVsizToplam.ToString();

                int toplam2 = faturaList.Sum(a => (int)a.ToplamTutar);//listeden veri çekme
                txtToplam.Text = toplam2.ToString();
              

                dataGridView1.DataSource = faturaList;

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LokantaDBEntities2 db = new LokantaDBEntities2();
            int id = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            Fatura ktp = db.Fatura.Where(a => a.ID == id).First();

            KisiDuzenle3 f = new KisiDuzenle3(ktp);
            f.Show();
            this.Hide();
        }
    }
}
