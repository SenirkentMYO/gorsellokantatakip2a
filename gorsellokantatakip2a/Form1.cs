﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gorsellokantatakip2a
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGiris_Click(object sender, EventArgs e)
        {
             List<Müsteri> kullanicilistesi = new List<Müsteri>();
           LokantaDBEntities2 database = new LokantaDBEntities2();

           kullanicilistesi = database.Müsteri.Where(a => a.Adi == txtAdi.Text | a.Soyadi == txtSoyadi.Text).ToList();
            

            if(kullanicilistesi.Count>0)
            {
                //List<Müsteri> MüsteriList = database.Müsteri.ToList();
                List<Müsteri> MüsteriList = database.Müsteri.Where(tablom => tablom.Adi.Contains(txtAdi.Text)).ToList();

                gvMüsteri2.DataSource = MüsteriList;
                //Form2 frm=new Form2();
               // frm.Show();

               // this.Hide();
            }
            else
            {
            lblHata2.Text="Aradığınız Müşteri bulunamadı!!!";
            }

            }

        private void lbnSonkayit_Click(object sender, EventArgs e)
        {
            using (LokantaDBEntities2 dbase = new LokantaDBEntities2())
            {
                //List<VwKitap> kitapList = dbase.VwKitap.ToList();
               // List<Müsteri> liste = dbase.Müsteri.Last(1).ToList();
                List<Müsteri> liste = dbase.Müsteri.OrderByDescending(a => a.ID).Take(1).ToList();
                

                gvMüsteri2.DataSource = liste;
            }
        }

        private void btnDuzen_Click(object sender, EventArgs e)
        {
            LokantaDBEntities2 db = new LokantaDBEntities2();
            int id = int.Parse(gvMüsteri2.SelectedRows[0].Cells[0].Value.ToString());
            Müsteri ba = db.Müsteri.Where(a => a.ID == id).First();

            KisiDuzenle2 f = new KisiDuzenle2(ba);
            f.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

  