﻿namespace gorsellokantatakip2a
{
    partial class MusteriEkle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNo = new System.Windows.Forms.Label();
            this.lblAd = new System.Windows.Forms.Label();
            this.lblSoyad = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblTarih = new System.Windows.Forms.Label();
            this.textNo = new System.Windows.Forms.TextBox();
            this.textAd = new System.Windows.Forms.TextBox();
            this.textSoyad = new System.Windows.Forms.TextBox();
            this.textTel = new System.Windows.Forms.TextBox();
            this.textTarih = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblMail = new System.Windows.Forms.Label();
            this.lblAdres = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblNo.Location = new System.Drawing.Point(66, 31);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(85, 20);
            this.lblNo.TabIndex = 0;
            this.lblNo.Text = "Müşteri No";
            this.lblNo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblNo.Click += new System.EventHandler(this.lblNo_Click);
            // 
            // lblAd
            // 
            this.lblAd.AutoSize = true;
            this.lblAd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAd.Location = new System.Drawing.Point(119, 68);
            this.lblAd.Name = "lblAd";
            this.lblAd.Size = new System.Drawing.Size(32, 20);
            this.lblAd.TabIndex = 1;
            this.lblAd.Text = "Adı";
            this.lblAd.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblSoyad
            // 
            this.lblSoyad.AutoSize = true;
            this.lblSoyad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSoyad.Location = new System.Drawing.Point(94, 94);
            this.lblSoyad.Name = "lblSoyad";
            this.lblSoyad.Size = new System.Drawing.Size(57, 20);
            this.lblSoyad.TabIndex = 2;
            this.lblSoyad.Text = "Soyadı";
            // 
            // lblTel
            // 
            this.lblTel.AutoSize = true;
            this.lblTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTel.Location = new System.Drawing.Point(97, 128);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(54, 20);
            this.lblTel.TabIndex = 3;
            this.lblTel.Text = "Tel No";
            // 
            // lblTarih
            // 
            this.lblTarih.AutoSize = true;
            this.lblTarih.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTarih.Location = new System.Drawing.Point(66, 160);
            this.lblTarih.Name = "lblTarih";
            this.lblTarih.Size = new System.Drawing.Size(85, 20);
            this.lblTarih.TabIndex = 4;
            this.lblTarih.Text = "Kayıt Tarihi";
            // 
            // textNo
            // 
            this.textNo.Location = new System.Drawing.Point(173, 33);
            this.textNo.Name = "textNo";
            this.textNo.Size = new System.Drawing.Size(132, 20);
            this.textNo.TabIndex = 5;
            // 
            // textAd
            // 
            this.textAd.Location = new System.Drawing.Point(173, 70);
            this.textAd.Name = "textAd";
            this.textAd.Size = new System.Drawing.Size(132, 20);
            this.textAd.TabIndex = 6;
            // 
            // textSoyad
            // 
            this.textSoyad.Location = new System.Drawing.Point(173, 96);
            this.textSoyad.Name = "textSoyad";
            this.textSoyad.Size = new System.Drawing.Size(132, 20);
            this.textSoyad.TabIndex = 7;
            // 
            // textTel
            // 
            this.textTel.Location = new System.Drawing.Point(173, 128);
            this.textTel.Name = "textTel";
            this.textTel.Size = new System.Drawing.Size(132, 20);
            this.textTel.TabIndex = 8;
            // 
            // textTarih
            // 
            this.textTarih.Location = new System.Drawing.Point(173, 160);
            this.textTarih.Name = "textTarih";
            this.textTarih.Size = new System.Drawing.Size(132, 20);
            this.textTarih.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(426, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(208, 20);
            this.textBox1.TabIndex = 10;
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMail.Location = new System.Drawing.Point(356, 33);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(53, 20);
            this.lblMail.TabIndex = 12;
            this.lblMail.Text = "E-Mail";
            this.lblMail.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lblAdres
            // 
            this.lblAdres.AutoSize = true;
            this.lblAdres.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAdres.Location = new System.Drawing.Point(356, 70);
            this.lblAdres.Name = "lblAdres";
            this.lblAdres.Size = new System.Drawing.Size(51, 20);
            this.lblAdres.TabIndex = 13;
            this.lblAdres.Text = "Adres";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(426, 68);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(208, 80);
            this.textBox2.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(426, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 53);
            this.button1.TabIndex = 15;
            this.button1.Text = "KAYDET";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // MusteriEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 262);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lblAdres);
            this.Controls.Add(this.lblMail);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textTarih);
            this.Controls.Add(this.textTel);
            this.Controls.Add(this.textSoyad);
            this.Controls.Add(this.textAd);
            this.Controls.Add(this.textNo);
            this.Controls.Add(this.lblTarih);
            this.Controls.Add(this.lblTel);
            this.Controls.Add(this.lblSoyad);
            this.Controls.Add(this.lblAd);
            this.Controls.Add(this.lblNo);
            this.Name = "MusteriEkle";
            this.Text = "MusteriEkle";
            this.Load += new System.EventHandler(this.MusteriEkle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.Label lblAd;
        private System.Windows.Forms.Label lblSoyad;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblTarih;
        private System.Windows.Forms.TextBox textNo;
        private System.Windows.Forms.TextBox textAd;
        private System.Windows.Forms.TextBox textSoyad;
        private System.Windows.Forms.TextBox textTel;
        private System.Windows.Forms.TextBox textTarih;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.Label lblAdres;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;

    }
}